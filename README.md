# Atom One Chroma

> [Chroma](https://github.com/alecthomas/chroma) compatible stylesheets using Atom's lovely [One Dark](https://atom.io/themes/one-dark-syntax) and [One Light](https://atom.io/themes/one-light-syntax) syntax themes.

[![NPM downloads per month](https://img.shields.io/npm/dm/atom-one-chroma.svg?style=flat-square)](https://www.npmjs.com/package/atom-one-chroma)
[![Latest NPM version](https://img.shields.io/npm/v/atom-one-chroma.svg?style=flat-square)](https://www.npmjs.com/package/atom-one-chroma)
[![Commit methodology](https://img.shields.io/badge/commits-conventional-yellow.svg?style=flat-square&longCache=true)](https://conventionalcommits.org)
[![WTFPL licensed](https://img.shields.io/npm/l/atom-one-chroma.svg?style=flat-square&longCache=true)](https:/codeberg.org/vhs/atom-one-chroma/src/branch/master/COPYING)

## Usage

Get the stylesheets on [jsDelivr](https://www.jsdelivr.com/package/npm/atom-one-chroma) or consume the NPM package and access built stylesheets from `dist`.

## How it works

NPM `build` script copies LESS stylesheet files from GitHub repositories for [One Dark Syntax](https://github.com/atom/one-dark-syntax) and [One Light Syntax](https://github.com/atom/one-light-syntax) into a `temp` directory, at which point each of them are converted to vanilla CSS using LESS compiler, minified with [clean-css](https://github.com/jakubpawlowicz/clean-css) and output to the `dist` directory.

## Development

1. Clone this repository with `git clone`.
1. Install NPM dependencies with `npm install`.
1. Run `npm run build` to generate stylesheets.

See `dist` folder for light and dark Atom One stylesheets.

**Note:** By default, generated stylesheets assume your highlighted code sits in a container with class `highlight`, e.g.

```html
<div class="highlight">
  <pre>
    <code>...</code>
  </pre>
</div>
```

If you are not using `highlight` as your wrapper class change the wrapper class name in `styles.less` before building.

## Pygments support

Supports code snippets generated with **[Pygments](http://pygments.org/) 2.2.0 and earlier** though future versions may be supported.

## Credits

This tool was inspired by [Richard Leland](https://github.com/richleland), [Mihály Gyöngyösi](https://github.com/mgyongyosi) and [Alex Wood](https://github.com/awood) from their following creative works:

- https://github.com/mgyongyosi/OneDarkJekyll/
- https://github.com/richleland/pygments-css
- https://gist.github.com/awood/9651461

## License

Copyright (C) 2017–2018 VHS <0xc000007b@tutanota.com>

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
