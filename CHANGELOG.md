# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="2.0.0"></a>
# [2.0.0](https:/codeberg.org/vhs/atom-one-chroma/compare/v1.1.2...v2.0.0) (2018-07-09)



<a name="1.1.2"></a>
## [1.1.2](https:/codeberg.org/vhs/atom-one-pygments/compare/v1.1.1...v1.1.2) (2018-07-09)



<a name="1.1.1"></a>
## [1.1.1](https:/codeberg.org/vhs/atom-one-pygments/compare/v1.1.0...v1.1.1) (2018-03-22)


### Bug Fixes

* **$style:** normalize space after line number ([99456ea](https:/codeberg.org/vhs/atom-one-pygments/commit/99456ea))



<a name="1.1.0"></a>
# 1.1.0 (2018-03-22)


### Features

* **$compile:** add chroma support ([3ddac91](https:/codeberg.org/vhs/atom-one-pygments/commit/3ddac91))
* **package:** one-dot-oh ([a518c63](https:/codeberg.org/vhs/atom-one-pygments/commit/a518c63))
